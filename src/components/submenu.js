import React from "react";
import Radium from "radium";


class Submenu extends React.Component {
  render() {
    var submenu = this.props.submenu;
    var styles = {
      dropdownContent: {
        width: '200px',
        position: 'absolute',
        overflowY: 'hidden',
        left: '-1px',
        top: '0',
        borderLeft: '1px solid #787a7d',
        borderRight: '1px solid #787a7d',
        borderBottom: '1px solid #787a7d',
        
        overflow: 'hidden',
        maxHeight: '0',
        paddingTop: '47px',
        paddingBottom: '0',
        marginTop: '0',
        marginBottom: '0',
        msTransitionDuration: '0.3s',
        WebkitTransitionDuration: '0.3s',
        transitionDuration: '0.3s',
        msTransitionTimingFunction: 'cubic-bezier(0, 1, 0.5, 1)',
        WebkitTransitionTimingFunction: 'cubic-bezier(0, 1, 0.5)',
        ':hover': {
          msTransitionDuration: '0.3s',
          WebkitTransitionDuration: '0.3s',
          transitionDuration: '0.3s',
          msTransitionTimingFunction: 'ease-in',
          WebkitTransitionTimingFunction: 'ease-in',
          transitionTimingFunction: 'ease-in',
          maxHeight: '193px',
          
        }
      },
      listItem: {
        width: '170px',
        color: '#FFF',
        backgroundColor: '#1a1e24',
        padding: '15px',
        margin: '0',
        textDecoration: 'none',
        display: 'block',
        ':hover': {
          color: 'black',
          backgroundColor: '#FFF',
        }
      }
    };
    return (
      <div className="dropdown-content" style={styles.dropdownContent}>
        {submenu.map(function(listItem, i) {
          return <a href="#" key={i} style={styles.listItem}>{listItem.label}</a>;
        })}
      </div>
    );
  }
}
module.exports = Radium(Submenu);