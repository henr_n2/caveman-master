import React from "react";
import Radium from "radium";

import Button from "./button";
import Submenu from "./submenu";

class Dropdown extends React.Component {

  render() {
    var styles = {
      dropdown: {
        position: 'relative',
        display: 'inline-block',
        border: '1px solid #787a7d',
        marginLeft: "100px",
        ':hover': {

        }
      },
      btn: {
        width: '200px',
        color: '#fff',
        fontSize: '14px',
        padding: '15px',
        border: 'none',
        backgroundColor: '#1a1e24',
        outline: 'none',
        cursor: 'pointer',
        textAlign: 'left',
        listStyle: 'none',
        ':hover': {
        }
      },
      arrow: {
        position: 'absolute',
        top: '16px',
        right: '10px',
        width: '14px',
        height: '14px',
      }
      
    
    };
    return (
      <div key="dropdown" className="dropdown" style={styles.dropdown}>
        <Button styling={styles.btn} label={this.props.label} className="dropbtn active"/>
        {Radium.getState(this.state, 'dropdown', ':hover') ? (
          <img src="/assets/images/arrow_up.svg" alt="up" style={styles.arrow} />
        ) : <img src="/assets/images/arrow_down.svg" alt="down" style={styles.arrow}/>}
        
          <Submenu className="dropdown-menu" submenu={this.props.submenu}/>
        
        
      </div>
    );
  }
}
module.exports = Radium(Dropdown);