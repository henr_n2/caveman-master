import React from "react";
import Radium from "radium";


class Button extends React.Component {

  render() {
    
    return (
      <button style={this.props.styling} className={this.props.className}>
        {this.props.label}
      </button>
    );
  }
}
module.exports = Radium(Button);