import React from 'react';
import Radium from "radium";

import Button from "./components/button";
import Dropdown from "./components/dropdown";
import Li from "./components/dropdown";

export default class App extends React.Component {
  constructor() {
  	super();
    this.state = {
		label: "view brand",
		label2: "Sort by",
		submenu: [
			{label: "My brand new portfolio"},
			{label: "Best profile match"},
			{label: "Highest earnings"},
			{label: "Date added"}
		],
		btn: {
			width: '200px',
			color: '#fff',
			fontSize: '16px',
			padding: '20px 0',
			backgroundColor: '#1a1e24',
			textTransform: 'uppercase',
			border: '1px solid #787a7d', 
			outline: 'none',
			cursor: 'pointer',
			boxShadow: 'inset 0 0 0 0 #fff',
			WebkitTransition: 'all ease 0.8s',
			msTransition: 'all ease 0.8s',
			transition: 'all ease 0.8s',
			':hover': {
				color: '#383d42',
				boxShadow: 'inset 0 -100px 0 0 #fff'
			},
			':active': {
				border: '1px solid #fff'
			}
			
		}
    };
  }
  render() {
    return (
    	<div>
    		<Button styling={this.state.btn} label={this.state.label} className="btn"/>
    		<Dropdown label={this.state.label2} className="" submenu={this.state.submenu}/>
   		</div>
   	);
  }
}
