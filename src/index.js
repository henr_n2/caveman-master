import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import Radium from "radium";
Radium.TestMode.enable();
ReactDOM.render(<App />, document.getElementById('app'));

// console.log('Good luck');
// console.log('thx!');
